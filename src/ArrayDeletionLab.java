import java.util.Arrays;
public class ArrayDeletionLab {
    static int[] arr = {1,2,3,4,5};
    static int Newarr[] = new int[arr.length-1];
    public static void main(String[] args) {
        int index = 2;
        int Value = 4;
        deleteElementByIndex(arr, index);
        deleteElementByValue(Newarr, Value);
    }
   
    static int[] deleteElementByIndex(int[] arr, int index){
        //create copy array
        for(int i=0; i<index; i++){
            Newarr[i] = arr[i];
        }
        //delete array
        for(int i=index+1; i<arr.length; i++){
            Newarr[i-1] = arr[i];
        }
        for(int i=0; i<Newarr.length; i++){
            System.out.print(Newarr[i]+" ");
        }
        System.out.println();
        return Newarr;
    }

    static void deleteElementByValue(int[] Newarr, int Value){
        int newIndex = 0;
        // Find the index of the value to delete
        for (int i=0; i<Newarr.length; i++) {
            if (Newarr[i] == Value) {
                newIndex = i;
                break;
            }
        }
            // Delete the value from the array
        for (int i=newIndex; i<Newarr.length - 1; i++) {
            Newarr[i] = Newarr[i + 1];
        }
        // array length-1
        Newarr = Arrays.copyOf(Newarr, Newarr.length - 1);

        for(int i=0; i<Newarr.length; i++){
            System.out.print(Newarr[i]+" ");
        }
        System.out.println();
    }
}
