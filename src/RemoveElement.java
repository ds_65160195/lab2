public class RemoveElement {
    public static void main(String[] args) {
        int nums[] = {0,1,2,2,3,0,4,2};
        int val = 2;
        int k = Change(nums,val);
        System.out.println(k);
        System.out.print("Nums = ");
        for(int i=0; i<nums.length; i++){
            if(i >= k){
                System.out.print("_ ");
            }else{
                System.out.print(nums[i]+" ");
            }
        }
        System.out.println();  
    }

    static int Change(int[] nums, int val) {
        int k = 0;
        for(int i=0; i<nums.length; i++){
            if(nums[i] != val){
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

}
